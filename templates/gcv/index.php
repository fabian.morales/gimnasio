<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  Templates.protostar
 *
 * @copyright   Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;
@ini_set('display_errors', '1');
// Getting params from template
$params = JFactory::getApplication()->getTemplate(true)->params;

$app = JFactory::getApplication();
$doc = JFactory::getDocument();
$this->language = $doc->language;
$this->direction = $doc->direction;

// Detecting Active Variables
$option   = $app->input->getCmd('option', '');
$view     = $app->input->getCmd('view', '');
$layout   = $app->input->getCmd('layout', '');
$task     = $app->input->getCmd('task', '');
$itemid   = $app->input->getCmd('Itemid', '');
$sitename = $app->getCfg('sitename');

if($task == "edit" || $layout == "form" )
{
	$fullWidth = 1;
}
else
{
	$fullWidth = 0;
}

// Add Stylesheets
JHtml::_('jquery.framework', true, true);
//$doc->addScript(JURI::root(true).'/media/jui/js/jquery.min.js');
$doc->addStyleSheet('templates/'.$this->template.'/foundation/css/foundation.min.css');
$doc->addStyleSheet('templates/'.$this->template.'/js/lightSlider/css/lightslider.css');
$doc->addStyleSheet('templates/'.$this->template.'/js/featherlight/featherlight.css');
$doc->addStyleSheet('templates/'.$this->template.'/css/style.css');

$doc->addScript('templates/'.$this->template.'/foundation/js/foundation.min.js');
$doc->addScript('templates/'.$this->template.'/foundation/js/vendor/modernizr.js');
$doc->addScript('templates/'.$this->template.'/js/scripts.js');
$doc->addScript('templates/'.$this->template.'/js/lightSlider/js/lightslider.min.js');
$doc->addScript('templates/'.$this->template.'/js/featherlight/featherlight.js');
$doc->addScript('templates/'.$this->template.'/js/jquery.rwdImageMaps.min.js');
// Add current user information
$user = JFactory::getUser();
$menu = $app->getMenu();
?>
<?php if(!$params->get('html5', 0)): ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<?php else: ?>
	<?php echo '<!DOCTYPE html>'; ?>
<?php endif; ?>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php echo $this->language; ?>" lang="<?php echo $this->language; ?>" dir="<?php echo $this->direction; ?>">
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <jdoc:include type="head" />
        <script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>

	<!--[if lt IE 9]>
		<script src="<?php echo $this->baseurl ?>/media/jui/js/html5.js"></script>
	<![endif]-->
</head>
<body>
    <div id="top-link"></div>
    <div id="loader"><div></div></div>
    <header>
        <div id="top-menu-resp" class="menu-resp">
            <a href="#"></a>
        </div>
        <div id="sidebar-menu">
            <a href="#" id="close"></a>
            <jdoc:include type="modules" name="sidebar-menu" style="xhtml" />
        </div>
        <div class="row">
            <div class="small-8 medium-5 columns">
                <div class="row">
                    <div class="small-3 columns">
                        <img src="templates/gcv/img/logo.png" title="Gimnasio Central del Valle" class="left" />        
                    </div>
                    <div class="small-9 columns">
                        <div class="logo left">
                            <span>Colegio Diocesano</span><br />
                            <span><big>Gimnasio Central del Valle</big></span><br />
                            <span><strong>Buga</strong></span>
                        </div>  
                    </div>
                </div>
            </div>
            <div id="top-menu" class="small-4 medium-7 columns">
                <jdoc:include type="modules" name="top-menu" style="xhtml" />
            </div>
        </div>
    </header>
    
    <div class="row fullWidth collapse banner">
        <div class="small-12 columns">
            <jdoc:include type="modules" name="banner" style="xhtml" />
        </div>
    </div>
    
    <div class="row">
        <div class="small-12 columns">
            <jdoc:include type="modules" name="sub-banner" style="xhtml" />
        </div>
    </div>
    
    <?php if($this->countModules('content-1') || $this->countModules('content-2')) : ?>
    <div class="row collapse gris">        
        <div class="<?php if($this->countModules('content-2')) : ?> medium-6 <?php else: ?> medium-12 <?php endif; ?> columns">
            <div class="modulo">
                <jdoc:include type="modules" name="content-1" style="xhtml" />
            </div>
        </div>
        <div class="<?php if($this->countModules('content-1')) : ?> medium-6 <?php else: ?> medium-12 <?php endif; ?> columns">
            <div class="modulo">
                <jdoc:include type="modules" name="content-2" style="xhtml" />
            </div>
        </div>
    </div>
    <?php endif; ?>
    
    <?php
        if ($menu->getActive() != $menu->getDefault()) : ?>
        <div class="component">
            <div class="row">
                <div class="small-12 columns">
                    <jdoc:include type="component" />
                </div>
            </div>
        </div>        
    <?php endif; ?>
    
    <?php if($this->countModules('video')) : ?>
    <div class="row">
        <div class="small-12 columns">
            <div class="seccion media cyan">
                Videos
            </div>
        </div>
        <div class="small-12 columns">
            <jdoc:include type="modules" name="video" style="xhtml" />
        </div>
    </div>
    <?php endif; ?>
    
    <?php if($this->countModules('news')) : ?>
    <div class="row">
        <div class="small-12 columns">
            <div class="seccion media cyan">
                Noticias y eventos
            </div>
        </div>
    </div>
    
    <div class="row">
        <div class="small-12 columns">
            <jdoc:include type="modules" name="news" style="xhtml" />
        </div>
    </div>
    <?php endif; ?>
    
    <div class="separador mini"></div>
    <footer>
        <div class="row">
            <div class="small-12 medium-6 columns logos">
                <img src="templates/gcv/img/eslogan.jpg" class="left" />
                <img src="templates/gcv/img/icontec.jpg" class="left" />
                <img src="templates/gcv/img/iq.png" class="left" />
            </div>
            <div class="small-12 medium-6 columns">
                <jdoc:include type="modules" name="footer" style="xhtml" />
            </div>
        </div>
        <div class="separador show-for-small-only"></div>
    </footer>
    
    <?php if($this->countModules('sidebar-left')) : ?>
    <div class="row">
        <div class="small-7 columns">
            <jdoc:include type="modules" name="sidebar-left" style="xhtml" />
        </div>
    </div>
    <?php endif; ?>
</body>
</html>
