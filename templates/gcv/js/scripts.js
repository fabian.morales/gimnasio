(function(window, $){         
    function scrollTo(target) {
        //var wheight = $(window).height();
        
        var ooo = $(target).offset().top;
        $('html, body').animate({scrollTop:ooo}, 600);
    }
    
    $(document).ready(function() {
        $("#top-menu-resp > a").click(function(e){
            e.preventDefault();
            $("#sidebar-menu").show("slow");
            $("#top-menu-resp").hide("slow");
        });
        
        $("#sidebar-menu #close").click(function(e) {
            e.preventDefault();
            $("#sidebar-menu").hide("slow");
            $("#top-menu-resp").show("slow");
        });
        
        /*$("#lnk_top").click(function(e){
            e.preventDefault();
            scrollTo("#top-link");
        });*/
                
        $("#banner, #slider_estudiantes").lightSlider({
            item: 1,
            pager: true,
            enableDrag: true,
            controls: true,
            loop: true
        });
                
        $(document).foundation();
	});
})(window, jQuery);